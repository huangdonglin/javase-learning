// 抽象类练习老师的案例

class Test1_Abstract {
	public static void main(String[] args) {
		BaseTeacher bt = new BaseTeacher("小明",18);
		bt.setName("小红");         // 可任意通过这种方式修改信息。
		bt.teach();
	}
}
/* 
* 具体事务： 基础班老师，就业班老师
	* 共性：姓名，年龄，讲课(具体讲课方式，内容都不确定，所以定义为抽象的)
*/
abstract class Teacher {
	private String name;
	private int age;

	public Teacher() {}

	public Teacher(String name,int age) {
		this.name = name;
		this.age = age;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getAge() {
		return age;
	}

	public abstract void teach() ;   // 抽象教学方法
}

class BaseTeacher extends Teacher{
	public BaseTeacher() {}

	public BaseTeacher(String name,int age) {
		super(name,age);
	}

	public void teach() {     // 重写方法的类型必须是和原方法类型一致才行。
		System.out.println("我的姓名是：" + this.getName() + "我今年：" + this.getAge() + " 讲的内容是java基础");
	}
}