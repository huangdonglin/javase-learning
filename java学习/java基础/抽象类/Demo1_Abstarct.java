class Demo1_Abstarct {
	public static void main(String[] args) {
		// Animal a = new Animal();  错误：Animal是抽象的，无法实例化。
		Animal a = new Cat();    // 不能直接实例化，通过子类对它进行实例化。
		a.eat();
	}
}
/* 抽象类特点
	* a:抽象类和抽象方法 必须用abstract关键字修饰
	* b:抽象类不一定有抽象方法，有抽象方法的类一定是抽象类或者接口
	* c:抽象类不能实例化那么，抽象类如何实例化呢？	
		* 按照多态的方式，由具体的子类实例化。其实这也是多态的一种，抽象类多态。
	* d:抽象类的子类
		* 要么是抽象类(要么子类需要是抽象类)
		* 要么重写抽象类中的所有抽象方法(普通类继承抽象类，必须强制性的对里面的抽象方法进行重写) 一般都用这种方式。
*/
abstract class Animal {       //抽象类
	
	public Animal()	{
		System.out.println("父类空参构造");
	}

	public abstract void eat();   //抽象方法
}

class Cat extends Animal {
	public void eat() {              // 直接重写父类 eat() 方法就好了，不需要加 abstract ,有抽象方法，必须是抽象类。
		System.out.println("猫吃鱼");
	}
}
