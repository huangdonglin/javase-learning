// 当无法描述一个方法时，据把它定义为抽象的。

class Demo4_Abstract {
	public static void main(String[] args) {
		Cat c = new Cat("加菲",8);
		System.out.println(c.getName() + "..." + c.getAge());
		c.eat();
		c.catchMouse();

		Dog d = new Dog("八公",30);
		System.out.println(d.getName() + "..." + d.getAge());
		d.eat();
		d.lookHome();
	}
}
/*
*A: 案例演示
	* 具体事务；毛，狗
	* 共性： 姓名，年龄，吃饭
	* 猫的特性：抓老鼠
	* 狗的特性 : 看家
*/

abstract class Animal {
	private String name;
	private int age;

	public Animal() {}

	public Animal(String name,int age) {
		this.name = name;
		this.age = age;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getAge() {
		return age;
	}

	abstract public void eat();    // 动物吃饭不知道该怎么吃，所以定义为抽象的，类也要变成抽象的。
}

class Cat extends Animal {
	public Cat() {}

	public Cat(String name,int age) {
		super(name,age);
	}

	public void eat() {
		System.out.println("猫吃鱼");
	}

	public void catchMouse() {
		System.out.println("抓老鼠");
	}
}

class Dog extends Animal {
	public Dog() {}

	public Dog(String name,int age) {
		super(name,age);
	}

	public void eat() {
		System.out.println("狗吃肉");
	}

	public void lookHome() {
		System.out.println("看门");
	}
}