
class Test2_Abstract {	
	public static void main(String[] args) {
		A a = new B();     //  可行,父类引用指向子类的对象，创建子类对象可行
		a.print();


		C c = new A();    // 同样的方法放到 abstract定义的抽象类上面，就会报错： A是抽象类，无法实例化。
		c.print();
	}
}
/* 
* A:面试题1：
	* 一个抽象类如果没有抽象方法，可不可以定义为抽象类？如果可以，有什么意义？
	答：可以。 这么做的目的只有一个，就是不让其他类创建本类的对象，交给子类完成。
	哦~~！ 我好像明白过来了。 当时一直想的是，一直把 抽象的类当作是父类去看待了。
	既然创建关于抽象类的对象，就必须让抽象类当作子类，用多态去 使用父类的引用指向抽象类(子类)的对象。
	正常的普通类，可以成功父类创建子类的对象,但是 抽象类当作子类别的类想创建它的对象是，就会报错.
	意思是抽象类当作子类，其他类当作父类，多态的 父类引用指向子类对象创建子类对象 ，抽象类作为子类，不允许这样.
* B:面试题2
	* abstract不能和哪些关键字共存?
	答：1.和static :
		被abstract修饰的方法没有方法体 ，被static修饰的可以用类名调用，但是类名，调用抽象方法是没有意义的。
		2.和final
		被abstract修饰的方法强制子类重写
		被final修饰的 不让子类重写，所以它俩是矛盾的。
		3.和private
		被abstract修饰的方法强制子类重写
		被private 修饰的方法不让子类重写(无法继承私有的),被private修饰不让子类访问，所以他俩是矛盾的。
*/
class C {
	public C() {}
}


abstract class A extends C {
	public A(){}

	//public static abstract void word();  // 错误：非法的修饰符组合，abstract和static

	//public final abstract void www();    // 错误：非法的修饰符组合，abstract和final
	
	//private final abstract void rrr();   // // 错误：非法的修饰符组合，abstract和private
	
	public void print() {
		System.out.println("哈哈哈哈");
	}
}
/*
class B extends A {
	public B() {}
}
*/
