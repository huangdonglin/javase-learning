class Demo5_Char {
	public static void main(String[] args) {

		//<1> 指定char类型数据 ，当赋值为数字时，输出其ASKII码对应的内容。

		char c1 = 97;
		char c2 = 'a';
		//char c3 = "a";           //错误: 不兼容的类型: String无法转换为char
		System.out.println(c1);  //结果 a
		
		System.out.println(c2);  //结果 a 
	
		//System.out.println(c3);

		//<2> char类型可以存储一个中文汉字，java采用Unicode编码，且char类型类型占两个字节，中文的一个汉字也占两个字节。
		char c6 = '中' ;
		System.out.println(c6);

		

	}
}
