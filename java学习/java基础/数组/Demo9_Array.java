// 案例演示 ： 数组查表法(根据键盘录入索引，查找对应的星期)

import java.util.Scanner;
class Demo9_Array {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("请输入对应的星期范围是1-7：");
		int week = sc.nextInt();
		System.out.println("星期" + getWeek(week));
	}

	public static char getWeek(int week) {
		char[] arr = {' ','一','二','三','四','五','六','七'};
		return arr[week];
	}
}
