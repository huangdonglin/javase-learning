
// 案例 获取数组中的最大值和最小值
class Demo6_Array {
	public static void main(String[] args) {
		int[] arr = {33,11,44,12,55,66};
		getMax(arr);
	}
	public static void getMax(int[] arr) {
		int max = arr[0];
		for (int i = 1;i < arr.length ;i++ ) {
			if (max < arr[i]) {
				max = arr[i];
			}
		}
		System.out.println(max);
		return;
	}
}
