// 案例数组的反转

class Demo7_Array {
	public static void main(String[] args) {
		int[] arr = {11,22,33,44,55};
		reverseArray(arr);
		range_arr(arr);
	}

// 反转函数
	public static void reverseArray(int[] arr) {
		for (int i = 0;i < arr.length / 2 ;i++ ) {
			// arr[0]和arr[arr,length-1-0]交换
			// arr[1]和arr[arr,length-1-1]交换
			// arr[2]和arr[arr,length-1-2]交换
			//...

			// 定义一个中间变量用于交换
			int temp = arr[i];
			// 反转
			arr[i] = arr[arr.length -1 -i];
			arr[arr.length-1-i] = temp;

		}
		
	}
	public static void range_arr(int[] arr) {
		for (int i = 0;i <= arr.length - 1 ;i++ ) {
			System.out.println(arr[i]);
		}
	}
}
