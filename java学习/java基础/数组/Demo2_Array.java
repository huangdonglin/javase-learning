
// 三个引用。两个数组

class Demo2_Array {
	public static void main(String[] args) {
		// 在这里创建了三个数组引用 arr1,2,3 ,但是只有两个实体数组
		// arr2 和 arr3 会是同一个数组吗？
		
		int[] arr1 = new int[3];
		int[] arr2 = new int[5];
		int[] arr3 = arr2;
		
		// 首先打印它们的地址,看看2,3 是否相同
		System.out.println(arr1);
		System.out.println(arr2);
		System.out.println(arr3);
		
		// 进行赋值
		arr1[0] = 10;
		arr1[1] = 20;

		arr2[1] = 30;
		arr3[1] = 40;
		arr3[2] = 50;

		System.out.println(arr1[0]);
		System.out.println(arr1[1]);
		System.out.println(arr1[2]);
		System.out.println("-----------------------------");
		System.out.println(arr2[0]);
		System.out.println(arr2[1]);
		System.out.println(arr2[2]);
		System.out.println(arr2[3]);
		System.out.println(arr2[4]);
		System.out.println("-----------------------------");
		System.out.println(arr3[0]);
		System.out.println(arr3[1]);
		System.out.println(arr3[2]);
		System.out.println(arr3[3]);
		System.out.println(arr3[4]);
		
/* 打印结果为：
[I@2a84aee7
[I@a09ee92        // 两个数组引用的地址值是相同的，证明是同一个数组，都指向堆中的一个实体数组
[I@a09ee92
10
20
0
-----------------------------
0
40   // 这里因为 arr2数组引用和arr3数组引用同一个地址值，所以操纵同一个数组
50	 // arr[2] = 30; 这一步操作就被下面的 arr[3]=40; 覆盖了。
0
0
-----------------------------
0
40
50
0
0
*/
	}
} 
