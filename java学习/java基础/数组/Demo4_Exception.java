/* 案例演示：
	1.ArrayIndexOutOfBoundsException:数组索引越界异常
		原因： 你访问了不存在的索引。
	2.NullPointerException：空指针异常
		* 元素： 数组已经不在指向堆内存了。而你还在用数组名去访问元素。
		* int[] arr = {1,2,3};
		* arr = null;
		* System.out.println(arr[0]);
*/

class Demo7_Exception {
	public static void main(String[] args) {
		int[] arr = new int[5];
		//System.out.println(arr[-1]);  1.你访问了不存在的索引
	
		arr = null;
		System.out.println(arr[0]);    //2.当数组引用赋值为null，再去调用数组中的元素就会出现空指针异常
	
	}
}