/* 二维数组概述
1.二维数组的格式
	* int[][] arr = new int[3][2];
2.
3.注意事项：
	a. 以下格式也可以表示二维数组
		* 数据类型 数组名[][] = new 数据类型[m][n];
		* 数据类型[] 数组名[] = new 数据类型[m][n];
	b. 注意以下定义的去区别
	*	
		int x;
		int y;
		int x,y;

		int[] x;
		int[] y[];

		int[] x,y[];     x是一维数组，y是二维数组
*/
// 案例演示：定义二维数组，输出二维数组名称，一维数组名称，一个元素

class Demo10_2Array {
	public static void main(String[] args) {
		int[][] arr = new int[3][2];
		
		System.out.println(arr);     // 二维数组
		System.out.println(arr[0]);  // 二维数组中的第一个一维数组
		System.out.println(arr[0][0]);
		/* 结果       
		[[I@2a84aee7     // 二维数组的地址值
		[I@a09ee92		 // 一位数组的地址值
		0				 // 元素值
		*/

		int[][] arr1 = new int[3][];
		arr1[0] = new int[3];
		arr1[1] = new int[5];
		
		arr[1][0] = 5;
		arr[0][3] = 2;
		arr1[1][4] = 10;

		System.out.println(arr1[0]);
		System.out.println(arr1[1]);


	}
}
