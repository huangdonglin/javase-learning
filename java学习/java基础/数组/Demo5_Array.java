/* 
	数组的属性：arr.length  代表数组的长度
*/
class Demo5_Array {
	public static void main(String[] args) {
		int[] arr = {11,22,33,44,55};
		// 数组的遍历操作
		for (int i = 0;i <= arr.length - 1 ;i++ ) {
			System.out.println(arr[i]);
		}

		// 调用方法，遍历打印数组。
		int[] arr2 = {3,4,5};
		range_arr(arr2);
	}		
	// 下面写一个遍历数组的的方法：
	public static void range_arr(int[] arr) {
		for (int i = 0;i <= arr.length - 1 ;i++ ) {
			System.out.println(arr[i]);
		}
	

	}
}
