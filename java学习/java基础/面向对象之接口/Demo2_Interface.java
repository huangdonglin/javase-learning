class Demo2_Interface {
	public static void main(String[] args) {
		Demo d = new Demo();
		d.print();
		System.out.println(Inter.num);  // 能用类名访问得到结果 10 ，所以 说明默认前面还会有 static ，且公开的变量，前面还有一个默认的 public.

		//B b = new B();
		//b.print();
	}
}
/* 
* 成员变量：只能是常量，并且是静态的并公共的。
			* 默认修饰符 : public static final ,这三个修饰符 没有顺序，可以随便写。但是默认按照这个顺序写好点，习惯。
			* 建议： 自己手动给出。
	* 构造方法：接口没有构造方法。
	* 成员方法：只能是抽象方法。
			* 默认修饰符：public abstract
			* 建议：自己手动给出。
*/
interface Inter {
	int num = 10;

	// public Inter() {}  报错，接口中没有构造方法。
	// 因为实际上是 class Demo extends Object implements Inter ,默认继承Object，所以Demo 类访问的是object类内容，不会访问接口里面的。
	// public void println() {}   错误：接口抽象方法不能带有主体。

}

class Demo implements Inter {
	public Demo() {
		super();  // 默认继承Object类
	}
	
	public void print() {
		//num = 20;   报错 ：无法为最终变量 num 分配值，因为在接口中 默认定义的所有变量都是常量，都会默认加上 final
		System.out.println(num);
	}
}


/*  普通类继承可以这样修改 num值

class A {

	int num = 10;
	public A() {
		System.out.println(num);
	}

	public void ss() {
		System.out.println(num);
	}
	
}

class B extends A {
	public void print() {
		num = 20;
		System.out.println(num);
		super.ss();
	}
}
*/