class Demo1_interface {
	public static void main(String[] args) {
		// Inter i = new Inter();  报错：无法实例化。换个想 ，其实和我们的抽象类都是一样的，里面都是抽象方法，如果可以实例化了，就可以调用抽象方法，那也没意义。
		Inter i = new Demo();    // 使用多态的父类引用指向子类对象。进行实例化创建对象
		i.print();

		new Demo().print();      // 匿名对象，创建 接口Inter的子类的匿名对象，再调用事先重写好的子类里面的 print() 方法。
	}
}
/* 
A: 接口概述
	* 从狭义的角度上面讲，就是指Java中的 interface(接口)
	* 从广义的角度上面讲，对外提供规则的都是接口

B: 接口特点
	*a: 接口用关键字interface表示
		* interface 接口名 {}
	*b: 类实现接口用inplements表示
		*class 类名 implements 接口名 {}
	*c: 接口不能实例化
		* 那么，接口如何实例化呢？
		* 按照多态的方式进行实例化
	*d: 接口的子类
		* a:可以是抽象类。但是意义不大。因为抽象类也无法实例化。
		* b:可以是具体类。要重写接口中的所有抽象方法。(推荐方案)
C: 案例演示
	* 接口特点
*/ 
interface Inter {
	public abstract void print();      // 接口中的方法都是抽象的

}
class Demo implements Inter {
	public void print() {
		System.out.println("我是接口");
	}
}