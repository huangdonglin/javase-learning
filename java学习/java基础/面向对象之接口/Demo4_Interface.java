// 猫狗案例，加入跳高功能分析及代码实现

class Demo4_Interface {
	public static void main(String[] args) {
		System.out.println("Hello World!");
	}
}

/* 
* A: 案例演示
	* 动物类：姓名，年龄，吃饭，睡觉。
	* 猫和狗
	* 动物培训接口：跳高
*/
abstract class Animal {
	private String name;
	private int age;

	public Animal() {}

	public Animal(String name,int age) {
		this.name = name;
		this.age = age;
	}

	public void setName() {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setage() {
		this.age = age;
	}

	public int getAge() {
		return age;
	}
	
	public abstract void eat();

	public abstract void sleep();
}

// 下面还需要一个动物跳高接口
interface Jumping {
	public void jump();  
}

class Cat extends Animal {
	public Cat() {}

	public Cat(String name,int age){
		super(name,age);
	}

	public void eat() {
		System.out.println("猫吃鱼");
	}

	public void sleep() {
		System.out.println("蜷缩着睡");
	}
}

class JumpCat extends Cat implements Jumping {
	public void jump() {
		System.out.println("猫跳高");
	}
} 