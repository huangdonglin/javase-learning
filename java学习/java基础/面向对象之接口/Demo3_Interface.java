// 类与类，类与接口，接口与接口的关系

class Demo3_Interface {
	public static void main(String[] args) {
		System.out.println("Hello World!");
	}
}
/* 
*A: 类与类，类与接口，接口与接口的关系:
	a：类与类：
		* 继承关系，只能单继承，可以多层继承。
	b: 类与接口： 接口的出现打破了 单继承的局限性，可以做到多实现。
		* 实现关系，可以单实现，也可以多实现。
		* 并且还可以在继承一个类的同时实现多个接口。
	c: 接口与接口：
		* 继承关系，可以单继承，也可以多继承。
*/

class A {
}

interface InterA {
	public abstract  void printA();
}

interface InterB {
	public abstract void printB();

	public abstract void printA();   // 因为抽象方法是没有具体的方法体的。所以 InterA 和 InterB 可以同时含有 printA()方法，并不会造成安全隐患。
}

//interface InterC implements InterB {}   //报错： 接口与接口之间不能是实现关系。

interface InterC extends InterB,InterA {   // 接口与接口之间可以单继承，也可以多继承。
	
}


class Demo extends A implements InterA,InterB {    // 类与接口实现关系 ，既可以单实现，也可以多实现。
	public void printA() {
		System.out.println("printA");
	}

	public void printB() {
		System.out.println("printB");
	}
}
