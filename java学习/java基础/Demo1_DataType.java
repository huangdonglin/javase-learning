class Demo1_DataType {            // datatype 数据类型
	public static void main(String[] args) {
		// 整数类型
		byte b = 10;		// 占一个字节，-128 到 127
		short s = 20;		// 占两个字节
		int i = 30;			// 占四个字节(整数默认的数据类型就是int型)
		long x = 40;		// 占八个字节
		long y = 88888888888L;
		System.out.println(b);
		System.out.println(s);
		System.out.println(i);
		System.out.println(x);
		System.out.println(y);  // 默认为int型，会报错，接收不了这么大的整数。所以在后面加上一个 L(或者小写l),就可以使它变为long型。
		// 注意看下面这个面试题
		System.out.println(12345 + 5432l); 			// 答案是17777 ， 因为第二个数是 5432 ，后面不是数字一，而是字母L
	

		// 浮点型
		float f = 12.3f;		// 占四个字节
		double d = 33.4;	// 占八个字节	小数默认的是 double型
		
		System.out.println(f);  // 同样这里也会报错，默认f是double型数据，现在把它转换为小的 float型。就会造成损失精度。同理需要在后面加上一个 f或者F
		System.out.println(d);


		//字符类型
		char c = 'a';		//占两个字节
		System.out.println(c);


		//布尔类型
		boolean b1 = true;
		boolean b2 = false;
		System.out.println(b1);
		System.out.println(b2);
	}
}
