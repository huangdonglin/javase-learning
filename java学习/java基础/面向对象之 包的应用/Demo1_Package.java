/* 
*A :定义包的格式
	* package 包名;
	* 多级包用 .  分开即可
*B :定义包的注意事项
	* A：package语句必须是程序的第一条可执行的代码
	* B: package语句在一个java文件中只能有一个
	* C: 如果没有package,默认表示无包名
*/
/* 编译和运行带包类：
javac -d ./ Demo1_Package.java    手动创建执行包的时候，还有运行的时候就需要这两行代码
java com.heima.Demo1_Package
*/
package com.heima;
import com.xxx.Student;
class Demo1_Package {
	public static  void main(String[] args) {
		com.baidu.Person p = new com.baidu.Person("张三",23);
		System.out.println(p.getName() + "..." + p.getAge());

		//p.print();   // 在不同包下面的无关类，不允许访问，因为是protected修饰的。
		Student s = new Student("李四",24);
		System.out.println(s.getName() + "..." + s.getAge());
		s.method();       
	}
}

