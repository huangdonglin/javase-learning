//import 关键字的概述和使用
// import: 其实就是让有包的类对调用者可见，不用写全类名了

package com.heima;   // package 语句必须放在可执行代码的第一条
//import java.util.Scanner;
import com.baidu.Person;
class Demo2_Package {
	public static  void main(String[] args) {
		Person p = new Person("张三",23);
		System.out.println(p.getName() + "..." + p.getAge());

		java.util.Scanner sc = new java.util.Scanner(System.in);   // 这里也不需要前面的 import 可以直接这样进行使用
		int x = sc.nextInt();
		System.out.println(x);

	}
}
