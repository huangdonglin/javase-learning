
package com.baidu;
public class Person {   // 必须加上 public 使类变成公共的，其他外部程序包可以去调用它
	private String name;
	private int age;

	public Person(){}      // 构造方法前面依然要加上 public

	public Person(String name,int age){
		this.name = name;
		this.age = age;
	}
	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setAge() {
		this.age = age;
	}

	public int getAge() {
		return age;
	}

	protected void print() {
		System.out.println("hahaha");
	}
}

