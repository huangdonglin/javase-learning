package com.heima.bean;

public class Person {
	private String name;
	private int age;

	public Person() {     // alt + shiff + s + c  生成空參构造
		super();		
	}

	public String getName() {      // alt + shift + s 再 + r   生成 set 和 get 方法   
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
	
}
