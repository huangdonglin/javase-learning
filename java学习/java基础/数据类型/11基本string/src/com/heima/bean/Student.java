package com.heima.bean;

public class Student {
	private String name;
	private int age;
	
	public Student() {
		super();	
	}
	
	public Student(String name, int age) {
		super();
		this.name = name;
		this.age = age;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "Student [name=" + name + ", age=" + age + "]";
	}      // 快捷键 Ctrl + Alt + s   再+s ,直接创建 toString() 方法。 

	/*
	 * @Override public String toString() { //return super.toString(); return
	 * "我的姓名是：" + name + ",我的年龄是 ：" + age; }
	 */
	
	@Override
	// 重写equals方法，比较的是属性值。
	public boolean equals(Object obj) {
		Student s = (Student)obj;      // 这点复习参考多态的特性，需要向下转型才能调用 Student 里面的私有属性
		return this.name.equals(s.name)  && this.age ==  s.age;
	}
	
}
