package com.heima.object;

import com.heima.bean.Student;

public class Demo1_HashCode {
	public static void main (String[] args) {
		Object obj1 = new Object ();
		int hashCode = obj1.hashCode();
		System.out.println(hashCode);        // 创建对象，对象进内存，需要申请一个地址值，hashCode 就是一个地址值。
		
		Student s1 = new Student("张三",23);
		Student s2 = new Student("李四",24);
		
		System.out.println(s1.hashCode());       // Ctrl + Alt + 下键  可以直接向下复制一行 
		System.out.println(s2.hashCode());      // 每个对象都有自己的 hashCode（） 值。
	}
}
