package com.heima.object;

import com.heima.bean.Student;

public class Demo4_Equals {

	/** 
	 *  @param args
	 *  equals方法， 比较两个对象是否相等
	 *      public boolean equals(Object obj) {
        		return (this == obj);        // s1 将地址值赋给 this ,s2将地址值赋给obj
    		}
    		
    		Object中的equals方法是比较对象的地址值的，没有什么意义相当于直接比较：s1 == s2 ; 所以需要重写它。
    		因为在开发中我们通常比较的是对象中的属性值，我们认为相同属性是同一个对象，这样我们就需要重写equals方法
  
  	== 和 equals方法的区别：
  		共同点：返回值都是boolean 类型
  		区别：   1. ==号是比较运算符，既可以是比较基本的数据类型，也可以比较引用数据类型。基本数据类型比较的是值，引用数据类型比较的是地址值。
  					 2. equals方法 只能比较的是引用数据类型，没重写之前比较的是地址值，依赖的是底层（ ==），但是比较地址值是没有意义的，我们需要重写equals方法，比较对象中的属性值。
	 */
	
	public static void main(String[] args) {
		Student s1 = new Student("张三",23);
		Student s2 = new Student("张三",23);
		
		System.out.println(s1 == s2);
		// 重写之后
		boolean b = s1.equals(s2); 
		System.out.println(b);
	}

}
