package com.heima.object;

import com.heima.bean.Student;
/**
 *		@param args 
 *  	com.heima.bean.Student@279f2327
	 *     public String toString() {
	        return getClass().getName() + "@" + Integer.toHexString(hashCode());
	        
	        左边： 类名
	        中间：  @ 符号、
	        右边： hashCode的 16进制表现形式。
	        这么写没有实际意义，一般都是重写它。
	        
	        toString方法的作用：  更方便的显示属性值。
	        getXxx方法是为了获取值，可以显示也可以赋值，或者其他操作。
    }
 */

public class Demo3_ToString {

	public static void main(String[] args) {
		Student s1 = new Student("张三",23);
		String str = s1.toString();
		System.out.println(str);    // 或者 、
		System.out.println(s1.toString());   // 或者，
		System.out.println(s1);                  // 如果直接打印对象的引用就会直接默认调用 toString() 方法。
		
		
		
		// 此语句和toString 一样可以用来显示属性值； 但是toString() 只是为了显示属性结果（更快捷一些）。下面的方法可以进行使用操作属性值。
		System.out.println("我的姓名是：" + s1.getName() + ",我的年龄是 ：" + s1.getAge());
		
	}
	
}
