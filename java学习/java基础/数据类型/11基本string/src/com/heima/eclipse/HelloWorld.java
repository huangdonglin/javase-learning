package com.heima.eclipse;
import com.heima.animal.Cat;
import  com.heima.bean.Student;

public class HelloWorld {

	public static void main(String[] args) {
		System.out.println("Hello World!");
		Student s = new Student("zhang",23);
		System.out.println(s.getName() + "..." + s.getAge()) ;
		
		// 不同项目下，就是把它打包成一个jar 文件然后复制到此项目下然后使用。
		Cat cat = new Cat();
		cat.eat();
		cat.sleep();
 	}
}
