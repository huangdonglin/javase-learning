package com.heima.test;

import java.util.Scanner;

public class Test6 {
		
		/**
		 		*A：案例演示
		 			* 需求：把字符串反转
		 			* 
		 			*   分析：通过键盘录入库如获取字符串，变成字符数组。到这遍历字符数组并且再次拼接成为字符串。然后打印。
		 */
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("请输入一个字符串");
		String line = sc.nextLine();
		
		// 转换为字符数组
		char[] arr = line.toCharArray();
		
		String s = "";
		for (int i = arr.length-1; i >= 0; i--) {
			s = s + arr[i];
		}
		
		System.out.println(s);
	}

}
