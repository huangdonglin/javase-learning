package com.heima.test;

public class Test3 {
/*
 	*案例演示
 		* 需求：统计一个字符串中大写字母字符和小写字母字符，数字字符出现的次数，其他字符出现的次数。
 		*ABCDEabcd123456!@#$%^
 		*   分析：字符串是由字符组成的，而字符的值是有范围的，通过范围来判断是否包含该字符
 		*   如果包含就让计数器自增
 */
	public static void main(String[] args) {
		String s = "ABCDEabcd123456!@#$%^";
		int big = 0;
		int small = 0;
		int num = 0;
		int other = 0;
		// 1.通过for循环遍历每一个字符
		for(int i = 0;i < s.length() ;i++){
			char c = s.charAt(i);
		// 2. 判断字符是否在这个区域内
			if(c > 'A' && c <= 'Z') {
				big++;
			}else if(c >= 'a' && c <= 'z'){
				small++;
			}else if ( c >= '0' && c <= '9') {
				num++;
			}else {
				other++;
			}
		}
		//3.打印每一个计数器中的数值大小；
		System.out.println("big = " + big + "\nsmall = " +small + "\nnum = " + num +"\nother = " + other);
	}

}
