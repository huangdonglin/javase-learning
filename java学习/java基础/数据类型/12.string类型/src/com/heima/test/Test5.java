package com.heima.test;

public class Test5 {
		/*
		 * * A: 案例提示
		 *  需求：把数组中的数据按照指定格式拼接成一个字符串
		    	* 举例：
		    			* int[] arr = [1,2,3];
		   				* 输出结果：
		   					* "[1, 2, 3]"	   (就是输出这样类似数组样子的字符串)
		 */
	public static void main(String[] args) {
		int[] arr = {1,2,3};
		String s = "[";
		for (int i = 0; i < arr.length; i++) {
			if(i == arr.length -1 ) {
				s = s + arr[i] + "]";
			}else {
				s = s + arr[i] + ", ";
			}
		}
		System.out.println(s);
	}

}
