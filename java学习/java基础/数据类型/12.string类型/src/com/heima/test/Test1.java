package com.heima.test;

import java.util.Scanner;

/*
 *  案例演示： 模拟用户登陆，给三次机会，并提示还有几次。
 *  用户名和密码都是admin
 */
public class Test1 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in); // 创建键盘录入对象

		for (int i = 1; i <= 3; i++) {
			System.out.println("请输入用户名：");
			String userName = sc.nextLine();
			System.out.println("请输入密码");
			String passworld = sc.nextLine();

			// 如果字符串常量和字符串变量进行比较，通常都是字符串常量调用方法，将变量当做参数传递，防止空指针异常。
			if ("admin".equals(userName) && "admin".equals(passworld)) {
				System.out.println("欢迎" + userName + "登录！");
				break;
			} else {
				if (i == 3) {
					System.out.println("您的错误次数过多，账号已被冻结！");
				} else {
					System.out.println("用户名或者密码错误! \n	你还有 " + (3 - i) + "次机会重新输入：");
				}
			}
		}
	}
}
