package com.heima.string;

public class Demo6 {
		/**
		       * A: String的替换功能及案例演示
		       			*String replace(char old,char new)
		       			*String replace(String old,String new)
		       * B: String的去除字符串两端空格及案例演示
		       		* String trim()
		       	* C：String 的按字典顺序排序比较两个字符串及案例演示
		       			* int compareTo(String str) （暂时不用掌握）
		       			* int compareToIgnoreCase(String str) (了解)      // 不许分大小写的比较 
		 */
	public static void main(String[] args) {
		  //demo1();
		  //demo2();
		String s1 = "abc"; 
		String s2 = "bcd";
		
		int num = s1.compareTo(s2);               // 按照码表值进行比较
		System.out.println(num);
		
		String s3 = "黑";
		String s4 = "妞";
		int num2 = s2.compareTo(s3);              // 查找的是unicode码表值。
		System.out.println(num2);
				
	}

		private static void demo2() {
			String s = "		huang		dong	lin			";
			String s2 = s.trim();
			System.out.println(s2);
		}

		private static void demo1() {
			String s = "huangdonglin";
			  
			  String s2 = s.replace('g','G');          //  用G 替换 g
			  System.out.println(s2);
			  
			  String s3 = s.replace('z', 't');			//  如果被替换的字符在字符串中不存在，那么将不会替换 
			  System.out.println(s3);
			  
			  String s4 = s.replace("huang","wang");
			  System.out.println(s4);                 // 不存在的情况下也是不改变
		}

}
