package com.heima.string;
// 注意 String类的valueOf()方法可以把任意类型的数据转换成字符串。
public class Demo5_StringMethod {

	public static void main(String[] args) {
		//demo1();  
		//demo2();
		//demo3();
		//demo4();
	}

	// 字符串大小写的转换方法。
	private static void demo4() {
		String s1 = "HUANGDONGlin";
		String s3 = s1.toLowerCase();
		String s4 = s1.toUpperCase();    // 记得方法调用之后记得使用一个新的变量接收这个结果 
		
		System.out.println(s3);
		System.out.println(s4);
		
		// 字符串连接，+ 使用起来更加强大，可以用字符串·与任意类型的数据相加。
		System.out.println(s3.concat(s4));      // concat() 方法 调用的和传入的都只能是字符串。
	}

	private static void demo3() {
		char[] arr = {'a','b','c'};
		String s = String.valueOf(arr);    // 查看源码，底层就是由String类的构造方法完成的
		System.out.println(s);
		
		String s2 = String.valueOf(100);           // 将整形数字转换成为字符串类型100
		System.out.println(s2);
		
		// 还可以参数传进去 一个对象名，底层也是通过 tostring()方法实现的。就等于是打印这个对象的 对象名@分配的地址
	}

	// 将字符串转换成字符数组
	private static void demo2() {
		String s = "huangdonglin";
		 char[] arr = s.toCharArray();
		 for (int i = 0; i < arr.length; i++) {
			 System.out.print(arr[i] + " ");
			
		}
	}
	
	// 将字符串转换成字节数组
	private static void demo1() {
		String s1 = "abc";
		byte[] arr = s1.getBytes();
		for (int i = 0; i < arr.length; i++) {
			System.out.println(arr[i] + " ");
		}
		String s2 = "你好你好";
		byte[] arr2 = s2.getBytes();             // ͨ默认通过gbk码表将字符串转换成字节数组，可以鼠标右击 day12这个项目，然后下拉找到Properties,就可以进行设置更改。
		for (int j = 0; j < arr2.length; j++) {     //  编码：把我们看的懂得转换为计算机看的懂得
			System.out.print(arr2[j] +" ");       // gbk码表 一个中文代表两个字节
																 //  gbk码表的特点，中文的第一个字节肯定是负数。
		}
	}

}
