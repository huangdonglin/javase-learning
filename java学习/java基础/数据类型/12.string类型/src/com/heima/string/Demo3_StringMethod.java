package com.heima.string;

public class Demo3_StringMethod {

	/**
	 		* boolean equals(Object obj) : 比较字符串的内容是否相同，区分大小写。
	 		*  boolean equalsIgnoreCase(String str) : 比较两个字符串的内容是否相同，不区分大小写。
	 		*  boolean contains(String str) : 判断大字符串中是否包含小字符串
	 		*  boolean startsWith(String str) : 判断字符串是否以某个指定的字符串开头
	 		*  boolean endWith(String str) : 判断字符串是否已某个指定的字符串结尾
	 		*  boolean isEmpty() : 判断字符串是否为空。 
	 		*  
	 		*   "" 和 null的 区别；
	 		*   ""是字符串常量，同时也是一个String类的对象，就可以调用String类的方法。
	 		*   null 是空常量，不能用任何的方法，否则会出现空指针异常，null可以给任意的引用数据类型赋值。
	 */
	public static void main(String[] args) {
		//demo1();
		demo2();
		
		String s1 = "huang  don  glin";
		String s2 = "";
		String s3 = " ";
		String s4 = null;
		
		System.out.println(s1.isEmpty());       // false
		System.out.println(s2.isEmpty());       // true
		System.out.println(s3.isEmpty());       // false
		//System.out.println(s4.isEmpty());       // 报错：NullPointerException，null,表示空。说明此时s4没有记录任何字符串对象，所以调用字符串方法就会报错；
		 
	}

	private static void demo2() {
		String s1 = "我是huangdonglin哈哈";
		String s2 = "huangdonglin";
		String s3 = "baima";
		String s4 = "我是";
		String s5 = "哈哈";
		String s6 = "";
		
		// 判断是否包含传入字符串
		System.out.println(s1.contains(s2));       // ture         
		System.out.println(s1.contains(s3));       // false
		System.out.println("--------------------");
		
		// 判断是否已传入的字符串开头或结尾
		System.out.println(s1.startsWith(s4));     // true     
		System.out.println(s1.endsWith(s4));      // false
		System.out.println("hhhhhhhhhhhhhh");
		System.out.println(s1.endsWith(s6));      // true
	}

	private static void demo1() {
		String s1 = "heima";
		String s2 = "heima";
		String s3 = "Heima";
				
		System.out.println(s1.equals(s2));     // true
		System.out.println(s2.equals(s3));     // false
		System.out.println(s2.equalsIgnoreCase(s3));     // true
		
		System.out.println("--------------------");
	}
     
}
