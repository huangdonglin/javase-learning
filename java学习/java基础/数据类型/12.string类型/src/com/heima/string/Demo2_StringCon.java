package com.heima.string;
// 一些 String类的构造方法使用
public class Demo2_StringCon {

	public static void main(String[] args) {
		byte[] arr1 = {97,98,99};             // 解码，将计算机读得懂的转化成我们读得懂的。
		String s2 = new String(arr1);
		System.out.println(s2);
		
		byte[] arr2 = {97,98,99,100,101,102};
		String s3 = new String(arr2,2,3);   // 将arr2字节数组从2索引开始转换3个
		System.out.println(s3);
		
		char[] arr3 = {'a','b','c','d'};
		String s4 = new String(arr3);
		System.out.println(s4);
		
		String s5 = new String(arr3,1,3);
		System.out.println(s5);            // 将arr3字符数组，从1索开始转换后三个成字符串
		
		String s6 = new String("hello");
		System.out.println(s6);
	}

}
