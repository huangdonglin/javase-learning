package com.heima.string;
/*
 		*  int length(): 获取字符串长度。
 		* char charAt(int index) : 获取指定索引位置的字符。
		* int indexOf(int ch) : 返回指定字符在此字符串中第一次出现处的索引。
		* int indexOf(String str) :返回指定字符串在此字符串中第一次出现处的索引。
 		* int indexOf(int ch,int formIndex) :返回指定字符在此字符串中指定位置后第一次出现处的索引。
 		* int indexOf(String str,int fromIndex):   返回指定字符串在此字符串中指定位置后第一次出现处的索引。
 		* String substring(0,5);           // 和python的字符串切片操作一样。前闭后开
 */

public class Demo4_StringMethod {

	public static void main(String[] args) {
		
		//demo1();	
		//demo2();
		//demo3();
		//demo4();
		//demo5();
	}

	private static void demo5() {
		// 面试问题
		String s = "woaixuexi";
		s.substring(4);                 
		System.out.println(s);     // 结果还是woaixuexi ，因为字符串常量一旦被定义是不会修改的。 需要重新使用一个字符变量接收这个字符方法运算的字符结果。 
	}

	private static void demo4() {
		String s1 ="woshihuangdonglin";
		String s2 = s1.substring(5);              // 从指定位置，一直截取到末尾。
		System.out.println(s2);
		
		String s3 = s1.substring(0,5);           // 和python的字符串切片操作一样。前闭后开。
		System.out.println(s3);
	}

	private static void demo3() {
		String s1 = "woooshihuangdonglin";
		int index1 = s1.indexOf('o',3);         // 从第三个索引位置之后进行查找,包括第三个索引位置。
		System.out.println(index1);
		
		int index2 = s1.lastIndexOf('o') ;       // 从后向前找，第一次出现的字符，返回索引值,包括
		System.out.println(index2);
		
		int index3 = s1.lastIndexOf('o',13) ;    // 从后向前找(包括指定的位置)，第一次出现的字符，返回索引值,
		System.out.println(index3);
		
		// 找不到还是 返回-1
	}

	private static void demo2() {
		// 根据字符找到索引
		String s1 = "huangdonglin";
		
		int index = s1.indexOf(97);       //参数接受的是int类型，传递char|类型的会自动提升
		int index1 = s1.indexOf('a');
		int index2 = s1.indexOf('z');     
		
		int index3 = s1.indexOf("dong");
		int index4 = s1.indexOf("ding");
		
		System.out.println(index);
		System.out.println(index1);
		System.out.println(index2);     // 如果不存在返回的就是-1。
		System.out.println(index3);    // 获取字符串中第一个字符出现的位置
		System.out.println(index4);    // 同样搜索不到这个字符串 会 返回-1
	}

	private static void demo1() {
		// length()方法获取的是每一个字符的个数。 与数组中length不同，数组中的length是数组属性，String的length() 是方法。
		String s1 = "huangdonglin";
		String s2 = "黄东林 。。  haha";
		System.out.println(s1.length());     // 12
		System.out.println(s2.length());     // 12
		
		// 根据索引找到对应元素
		char c = s2.charAt(5);  
		System.out.println(c);
	}

}
