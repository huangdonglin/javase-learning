package com.heima.string;

public class Demo1_String {
	/**
	 *  a ： 字符串字面值 “abc”也可以看作是一个字符串对象。
	 *  b :   字符串是常量，一旦被赋值，就不能被改变。
	 */
	
	public static void main(String[] args) {
		// Person p = new Person();
		String str = "abc";       // "abc" 可以看作是一个字符串对象，指的是 “abc”对象本身，不能进行改变，也能没有任何改变内部属性值的方法去改变其对象内部属性值
		str = "def";       // 此时 str 指向 对象“def”, 对象“abc”没有被指引 将变成垃圾
		System.out.println(str);     // String类重写了 toSring方法，返回的是该对象本身
		
	}

}
