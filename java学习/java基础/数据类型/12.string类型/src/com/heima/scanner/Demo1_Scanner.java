package com.heima.scanner;
// hasNextXxx() 判断键盘录入数据类型的方法的使用 : 判断是否有下一个输入项，其中Xxx可以是Int,Double等. 如果需要判断是否包含下一个字符串，则可以省略Xxx
// nextXxx()      获取下一个输入项。 Xxx的含义二号上个方法的Xxx相同，默认情况下，Scanner使用空格，回车等作为分隔符

import java.util.Scanner;

public class Demo1_Scanner {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		/*
		 * int i = sc.nextInt(); // 键盘录入一个整数 System.out.println(i);
		 */
		if (sc.hasNextInt()) {      // hasNextInt() 判断输入的数据是否为一个int类型，是返回 true不是返回false
			int i = sc.nextInt(); // 键盘录入一个整数
			System.out.println(i);
		} else {
			System.out.println("输入类型错误");
		}
	}

}
