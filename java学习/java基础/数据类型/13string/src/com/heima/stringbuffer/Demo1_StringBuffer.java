package com.heima.stringbuffer;

public class Demo1_StringBuffer {
	
	/**
	 *   A : StringBuffer的构造方法：
	 		* public StringBuffer();   无参构造
	 		* public StringBuffer(int capacity); 指定容量的自负缓冲区对象、
	 		* public StringBuffer(String str); 指定字符换内容的字符串缓冲区对象
	 *	  B : StringBuffer的方法：
	 *			public int capacity() ; 返回当前容量 。     理论值（不掌握）
	 *			public int length();  	返回长度（字符数）。实际值
	 *	  C：案例演示
	 *			构造方法和成都长度方法的使用
	 */

	public static void main(String[] args) {

	}

}
