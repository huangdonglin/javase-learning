class Test1_DataTypeConversion {
	public static void main(String[] args) {
		// 面试题之变量相加和常量相加的区别
		byte b1 = 3;
		byte b2 = 4;
		byte b3 = b1 + b2;
		System.out.println(b3); 
		/* 编译的会后将会报错  错误: 不兼容的类型: 从int转换到byte可能会有损失
		   byte b3 = b1 + b2;
		   下面从两个方面分析一下原因：
		   1.byte与byte(或者shot,char)进行运算时候提升为int,两个int类型相加结果也是int。因为整数默认是int类型
		   2.b1与b2 是两个变量，变量存储的值是变化的，在编译的时候无法判断里面具体的值，相加有可能会超出byte的取值范围
		*/
		// 根据上面的理论看这个：
		byte b4 = 4 + 3;
		System.out.println(b4);  //正确输出结果7 ，按照理论 3 + 4 默认运算结果也是 int型的，为啥不报错有损失呢？
		
		// 原因：java编译器有常量优化机制，在编译的时候，就自动把 3 + 4的运算结果7，赋值给 byte4了，因为7可以用 byte类型数据表示，所以就可以报错。
	}
}
