/* 
A: this 关键字特点
	* 代表当前对象的引用
B: 案例演示
	* this的应用场景
	* 用来区分成员变量和局部变量的重名
*/


class Demo2_this {
	public static void main(String[] args) {
		Person p1 = new Person();
		p1.setAge(20);  
		System.out.println(p1.getAge());	

		p1.setName("张三");
		System.out.println(p1.getName());

	/* 当我们吧  set ,get方法里的变量都改成是age ，name时：
		此时输出结果是 0 和 null   原因：
		成员变量 和 方法里面的局部变量重名！ 根据java的就近原则，
		执行 name = name， age = age 的时候，其实都是在将方法内的局部变量赋值给此局部变量
		而 成员变量并没有被赋值！！！，所以输出成员变量的结果，都是默认值 0 和 null。
		这时候就需要加上一个 this ，代表当前对象的引用。其实和 python类似这些。只是python必须加 self,而java不重名是可以就近原则不报错。
	*/
	}   
}
class Person {
	private String name;
	private int age;

	public void setAge(int age) {
		if (age > 0 && age < 200) {
			this.age = age;
		}else {
			System.out.println("请回火星吧，地球不适合你！");
		}
		
	}

	public int getAge() {
		return age;			 // 这里可以省略 this. 系统默认加上
	}

	public void setName(String name) {    // 设置姓名
		this.name = name;
	}

	public String getName() {
		return name;
	}
}