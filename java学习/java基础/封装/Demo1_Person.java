/* 
	1. private 关键字特点
		* a:是一个权限修饰符
		* b:可以修饰成员变量和成员方法
		* c:被其修饰的成员只能在本类中进行访问

	2. 案例演示：封装和private的应用
		a. 把成员变量使用private修饰
		b. 提供对应的getXxx(),和setXxx()方法
		c. private仅仅是封装的一种体现形式，不能说封装就是私有
*/

class Demo1_Person {
	public static void main(String[] args) {
		Person p1 = new Person();
		p1.name = "张三";

		//p1.age = 20;       // 光明正大赋值不行，要对值有所限制。比如人的年龄在 0--200岁，超过这个范围的数字不能赋值。所以对成员变量 age 进行封装，提供 get和set 方法。
		//p1.speak();
		p1.setAge(-17);  
		System.out.println(p1.getAge());
		/* 输出结果为 ： 
		请回火星吧，地球不适合你！
		0

		因为并未给age赋值成功，所以输出它的默认值 0 
		*/
	}
}

class Person {
	private String name;
	private int age;

	public void speak() {
		System.out.println(name + "....." + age);
	}

	public void setAge(int a) {
		if (a > 0 && a < 200) {
			age = a;
		}else {
			System.out.println("请回火星吧，地球不适合你！");
		}
		
	}

	public int getAge() {
		return age;
	}
}