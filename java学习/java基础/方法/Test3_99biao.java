// 根据键盘录入的数据输出对应的乘法表 

import java.util.Scanner;
class Test3_99biao {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("请录入一个整数。范围在1-9之间");
		int num = sc.nextInt();

		print99(num);
	}

	public static void print99(int a) {
		for (int i = 1;i <= a ;i++ ) {
			for (int j = 1;j <= i ;j++ ) {
				System.out.print(j + "*" + i +"=" + (i * j) + "\t");
			} 
			System.out.println();
		}
	}
}
