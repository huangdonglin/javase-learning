
// 根据键盘录入的行数和列数，在控制台输出星型。

import java.util.Scanner;
class Test2 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("请输入行数");
		int row = sc.nextInt();
		System.out.println("请输入列数");
		int column = sc.nextInt();

		a(row,column);
	}
	public static void a(int x,int y) {
		for (int i = 1;i <= x ;i++ ) {
			for (int j = 1;j <= y ;j++ ) {
				System.out.print("*");
			}
			System.out.println();
		}

		return;    //如果返回值默认是void,return可以省略，即使省略系统也会默认给加上，形式是: return; 
	}
			
}
