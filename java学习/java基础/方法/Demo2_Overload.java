/*
		重载：方法名相同·参数列表不同，与返回值类型无关
		参数个数不同
		参数类型不同：
			包括参数顺序不同。
*/

/* 案例演示
	* 需求比较两个数据是否相等。
	* 参数类型分别是两个int型，两个double型，并在main方法中进行测试
*/
		
class Demo2_Overload {
	public static void main(String[] args) {
		boolean b1 = isEquals(10.0,10.0);
		System.out.println(b1);
	}

	public static boolean isEquals(int a,int b) {
		return a == b;
	}
	
	public static boolean isEquals(double a,double b) {
		return a == b;
	}
}
