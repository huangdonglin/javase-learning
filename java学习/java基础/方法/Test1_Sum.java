
import java.util.Scanner;
class Test1_Sum {
	public static void main(String[] args) {
		// 键盘录入两个数据返回其中的最大值和判断两个数字是否相等
		Scanner sc = new Scanner(System.in);  // 创建键盘录入对象
		System.out.println("请输入第一个整数");
		int x = sc.nextInt();
		System.out.println("请输入第二个整数");
		int y = sc.nextInt();

		int max = getMax(x,y);
		System.out.println(max);

		boolean c = isEquals(x,y);
		System.out.println(c);
	}
	// 创建一个函数，完成判断大小，返回最大值
	public static int getMax(int a,int b ) {
		return a > b ? a : b;
	}
	// 创建一个函数，判断两个数字是否相等
	public static boolean isEquals(int a,int b) {
		return a == b;
	}
}
