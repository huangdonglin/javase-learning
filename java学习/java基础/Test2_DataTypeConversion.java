class Test2_DataTypeConversion {
	public static void main(String[] args) {
		float f = 12.3f;
		long x = 12345;

		f = x;       // float 可以直接表示 long
		System.out.println(f);
		
        x = (long)f;       // 需要强制转换类型才可以，要不编译时报错，缺失精度。 
	}
}
// 老师经过一系列的讲解分析 float要比 long大，我也没细听。

