class Demo2_DataType {
	public static void main(String[] args) {
		/*
		* 1.作用于问题
			同一个区域不能使用相同的变量名
		* 2.初始化值问题
			局部变量在使用前必须赋值
		* 3.一条语句可以定义几个变量
			int a,b,c.....;
		*/
		int x = 10;
		int x = 20;  //会报错
		
		System.out.println(x);

		int y = 10;

		System.out.println(y);  //正常输出，不给y赋值。就会报错。

		int a = 5,b = 6,c = 7;  // 可以一行初始化和定义多个变量
		System.out.println(a);
	}
}
