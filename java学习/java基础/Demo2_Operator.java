class Demo2_Operator {
	public static void main(String[] args) {
		//自增，自减 ++ ，-- 用法和 C 一样，这里就不多介绍啦
		
		int a = 3;
		int b ;
		
		b = a++;
		System.out.println("a = " + a);  // 输出结果为 a = 4
		System.out.println("b = " + b);  // 输出结果为 b = 3
		int x = 4;

		int y = (x++) + (++x) + (x*10);
		//        4   +   6   +   60   = 70
		System.out.println(y);

		byte b = 10;
		b++;  //  b++ 等于 b = (byte)(b+1)
	}
}
