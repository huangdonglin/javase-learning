// 多态的好处和弊端

class Demo4_Animal {
	public static void main(String[] args) {
		Cat c1 = new Cat();
		c1.eat();
		// Animal a = new Cat();   开发中很少在创建对象的时候使用父类引用指向子类对象，直接创建子类对象更方便，可以使用子类中的特有属性和行为。 什么时候使用多态呢？当做参数的时候。
		
		// 创建多个对象太繁琐，所以抽出来一个方法。
		method(new Cat());
		
	  //method(new Dog());  // 相当于 ：Cat c = new Dog();  这是错误的，必须再给狗类定义一个方法
		method(new Dog());
		System.out.println("------------------");
		method(new Cat());
		method(new Dog());  // 相当于 Animal a = new Gog();  这样就提高了代码的扩展性。
	}
/*
	public static void method(Cat c) {
		c.eat();
	}

	public static void method(Dog d) {
		d.eat();
	}
*/
	// 假如还要创建其他类。比如猪，鸡。很麻烦，都要写一个这个方法。所以就用到了 多态
	public static void method(Animal a) {   // 当做参数的时候用多态最好，因为可扩展性强
		a.eat();  
		// 当我们想使用 子类中的特有方法和属性时，就需要一个关键字啦
		// 关键字：instanceof 判断前面的引用是否是后面的数据类型
		if (a instanceof Cat) {
			Cat c = (Cat)a;
			c.eat();
			c.catchMouse();
		}else if (a instanceof Dog) {
			Dog d = (Dog)a;
			d.eat();
			d.lookHome();
		}else {
			a.eat();    // 有了这个之后，开始的哪个 eat() 就不需要了
		}
	}
	
}
/* 
A:多态的好处
	*a: 提高了代码的维护性（继承保障）
	*b: 提高了代码的扩展性（由多态保证）
B:案例演示
	* 多态的好处
	* 可以当做形式参数，可以接收任意子类对象
C:多态的弊端
	* 不能使用子类特有的属性和行为。需要通过 向下转型。
*/

class Animal {
	public void eat() {
		System.out.println("动物吃饭");
	}
}

class Cat extends Animal {
	public void eat() {
		System.out.println("猫吃鱼");
	}

	public void catchMouse() {
		System.out.println("猫抓老鼠");
	}
}

class Dog extends Animal {
	public void eat() {
		System.out.println("狗吃肉");
	}

	public void lookHome() {
		System.out.println("狗看门");
	}
}

// 没事请查看视频 9.08 ：多态种的题目—分析题。