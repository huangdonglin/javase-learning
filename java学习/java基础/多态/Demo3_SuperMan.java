// 超人的故事案例：以及 多态中向上转型和向下转型   ： 查看图片，多态中上下转型
// 首先普通人 Json自我介绍 ，然后其实真正身份是超人在谈生意。然后发生危险，变成超人飞出去救人。


class Demo3_SuperMan {
	public static void main(String[] args) {
		Person p = new SuperMan();   // 父类引用指向子类对象，超人提升为了人
		System.out.println(p.name);  // 父类引用指向子类对象就是向上转型
		p.talkBusiness();
		
		SuperMan sm = (SuperMan)p;   // 向下转型。切记：我们引用数据类型一定先有向上转型，然后才有向下转型。比如本例，一定是 new SuperMan 先提升上去，下面才能再强转下来。
		sm.fly();
	}
}

class Person {
	String name = "John";
	public void talkBusiness() { // 谈生意
		System.out.println("谈生意");
	}
}

class SuperMan extends Person {
	String name = "superMan";
	
	public void talkBusiness() {
		System.out.println("谈几个亿的大单子");
	}

	public void fly() {
		System.out.println("飞出去救人");
	}
}