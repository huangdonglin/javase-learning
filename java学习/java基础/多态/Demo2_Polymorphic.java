// 多态中的成员访问特点之成员变量

class Demo2_Polymorphic {
	public static void main(String[] args) {
		Father f = new Son();     // 父类引用指向子类对象
		f.print();     // 和成员变量类似，但是执行到 f.print() 时，系统进行编译，先从父类中查找到 print() 方法，确定可以成功编译，然后再从子类中调用 print()方法进栈，如果父类没有 print() 方法，就会编译失败!报错。
					   //  编译时先看父类的 print（）方法，运行时走的是子类的print（）方法。这个也叫做动态绑定。
		f.println();   //  因为静态方法和静态变量是和类相关的。这里就相当于是 Father.println(); 
		
		Son s = new Son();
		System.out.println(s.num);
	}
}

// 成员变量 ： 编译看左边（父类），运行看左边（父类）。详细图解看图片：多态中的成员访问特点之成员变量
// 成员方法 ： 编译看左边（父类），运行看右边（子类）。
/* 
	静态方法：
	编译看左边（父类），运行看左边（父类）
	（静态是和类相关的，算不上是重写，所以访问还是左边的）
	只有非静态的成员方法，编译看左边，运行看右边
*/

class Father {
	int num = 10;

	public void print() {
		System.out.println("father");
	}

	public static void println() {
		System.out.println("Father static method");
	}
}

class Son extends Father {
	int num = 20;

	public void print() {
		System.out.println("son");
	}

	public static void println() {
		System.out.println("Son static method");
	}
}
