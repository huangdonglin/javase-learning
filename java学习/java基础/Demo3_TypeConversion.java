class Demo3_TypeConversion {			//Conversion 转换
	public static void main(String[] args) {
		//数据类型转换之隐式转换

		int x = 3;
		byte b = 4;
		
		x = x + b;  // 这个可以转换，byte变成int
		//b = x + b;  // 这个就会报错。因为 x + b 的结果运行起来，得到的是一个int型，int型数据赋值给 b(byte型)，就会造成损失。
		System.out.println(x);
		System.out.println(x);

		// 强制类型转换符
		b = (byte)(x + b);  //这样就不会报错了
		System.out.println(b);  

		// 强制类型转换之超出范围 。
		byte c = (byte)(126 + 4);
		System.out.println(c);    //结果为 -126
		/* 下面解释一下原因： 130 为int型，二进制形式为：
		00000000 00000000 00000000 100000010    130的二进制
		强制转换成 byte型为：
		10000010								这个结果就是 -126 的反码(因为数字在进行计算时，使用的是补码)
		原码 = 补码转换(除符号位置0变1,1变0) + 1
		11111110	    这个就是-126的 二进制码
		*/
	}
}