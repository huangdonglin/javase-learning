class Demo4_DataTypeConversion {     // 字符和字符串参与运算
	public static void main(String[] args) {
		System.out.println('a'+1);   // 运行结果为 98，这个和C语言一样，字符参与运算使用其ASKII所对应的值。

		System.out.println((char)99);  //结果就是c			

		System.out.println('a' + "hello" + 1);     // 任何数据类型用 + 与字符串相连接都会产生新的字符串。

		System.out.println("5 + 5 =" + 5 + 5  );    // 运算结果为 5 + 5=55
		
		System.out.println("5 + 5 =" + (5 + 5));    // 加上括号提升括号里面的优先级，结果就是 5 + 5=10


	} 
}
