class Demo1_Operator {
	public static void main(String[] args) {
		//(关系表达式) ？ 表达式1 ： 表达式2;
		int x = 10;
		int y = 5;
		int z ;
		z = (x > y) ? x : y;

		System.out.println(z);    //10

		// 判断两个数是否相等

		boolean v = (x == y) ? true : false;
		System.out.println("v = " + v);

		//获取三个整数中的最大值
		int a = 10;
		int b = 20;
		int c = 30;

		// 先比较任意两个数的值，最大值在与第三个数进行比较
		int temp = (a > b) ? a : b;
		int max = (temp > c) ? temp : c;
		System.out.println("max = " + max);

	}
}
