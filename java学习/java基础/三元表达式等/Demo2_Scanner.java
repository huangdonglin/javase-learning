
import java.util.Scanner;

class Demo2_Scanner {      //键盘录入的基本格式讲解
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);      // 创建键盘录入对象
		System.out.println("请输入第一个整数 :");
		int x = sc.nextInt();
		System.out.println(x);

		System.out.println("请输入第二个整数 :");
		int y = sc.nextInt();
		System.out.println(y);


	}
}
