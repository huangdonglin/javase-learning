import java.util.Scanner;

class  Test2_Switch {
	public static void main(String[] args) {
		// switch 使用场景：建议判断固定值的时候使用
		// if : 建议判断区间或范围的时候用

		// 下面案例演示 : 键盘输入月份，输出对应的季节
		/* 
		一年有四季
		3,4,5 春季
		6,7,8 夏季
		9,10,11 秋季
		12,1,2 冬季
		*/
		System.out.println("输入对应月份，查询季节");
		Scanner sc = new Scanner(System.in); //创建键盘对象
		
		int month = sc.nextInt();            //获取键盘输入对象，存在month中。
/*		
		switch (month) {
		case 3 :        // 利用case语句的穿透性，只在最后面加上一个输出语句，当结果为 3,4,5,是都会执行最后面的语句，简化代码
		case 4 :
		case 5 :
			System.out.println(month + "月是春季");
			break;
		case 6 :        
		case 7 :
		case 8 :
			System.out.println(month + "月是夏季");
			break;
		case 9 :        
		case 10:
		case 11:
			System.out.println(month + "月是秋季");
			break;
		case 12:        
		case 1 :
		case 2 :
			System.out.println(month + "月是冬季");
			break;
		default:
			System.out.println("对不起，没有对应季节");
*/
		// 下面再使用if语句来完成
		if (month > 12 || month < 1) {
			System.out.println("对不起没有对应的季节");
		}else if (month >= 3 && month <= 5) {
			System.out.println(month + "月是春季");
		}else if (month >= 6 && month <= 8) {
			System.out.println(month + "月是夏季");
		}else if (month >= 9 && month <= 11) {
			System.out.println(month + "月是秋季");
		}else {
			System.out.println(month + "月是冬季");
			}
	}
			
}
// 注意： switch语句都能用if语句做，但是有的if语句不能用switch来做。switch不能表示范围或者比较麻烦，比如分数判断80-100位优秀，switch需要case 20次，把每个分数case出来。
