
// 练习一，成绩判断

import java.util.Scanner;
class Test1_if {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		/*
		System.out.println("请输入学生成绩再1-100之间");
		int x = sc.nextInt();

		if (x >= 90 && x<= 100) {
			System.out.println("优");
		}else if (x >= 80 && x <= 89) {
			System.out.println("良");
		}else if (x >= 80 && x <= 89) {
			System.out.println("中");
		}else if (x >= 70 && x <= 79) {
			System.out.println("及格");
		}else if (x >= 60 && x <= 69) {
			System.out.println("差");
		}else {
			System.out.println("成绩录入错误");
			}
	*/
	// 根据所给值，进行选择计算
		System.out.println("请输入一个整数");
		int x = sc.nextInt();
		int y = 0;
		if (x >= 3) {
			y = 2 * x + 1;
		}else if (x > -1 && x < 3) {
			y = 2 * x;
		}else if (x <= -1) {
			y = 2 * x - 1;
		}

		System.out.println(y);
	}   

}
