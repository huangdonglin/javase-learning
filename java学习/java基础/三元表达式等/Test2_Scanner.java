
//从键盘输入两个数字并求和，然后也输出两个数字中的最大数字

import java.util.Scanner;

class Test2_Scanner {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("请输入第一个整数");
		int x = sc.nextInt();
		System.out.println("请输入第二个整数：");
		int y = sc.nextInt();
		int sum = x + y;
		System.out.println("两个数字之和为：");
		System.out.println(sum);
		int max = (x > y) ? x : y;
		System.out.println("max  ：");
		System.out.println(max);


	}
}
