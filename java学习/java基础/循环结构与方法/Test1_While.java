class Test1_While {
	public static void main(String[] args) {
		/* 
		A. 求1-100的和
		B. 统计“水仙花数”共有多少个
		*/
		int sum = 0;
		int i = 1;
		while (i <= 100) {
			sum += i;
			i++;
		}
		System.out.println("sum = "+ sum);
		
		// B
		int count = 0;
		int n = 100;
		while (n <= 999) {
			int ge = n % 10;
			int shi = n / 10 % 10;
			int bai = n / 100 % 10;
			if ((ge*ge*ge + shi*shi*shi + bai*bai*bai) == n ) {          // java里面没有立方的运算符
				count++;
			}
			n++;
		}	
		System.out.println("Count = "+ count);
	}
}

// 注意误区
/*

while () {      // 括号后面千万别加 冒号！！要不人就死循环有可能。
}

*/