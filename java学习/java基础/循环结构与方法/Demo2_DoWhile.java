class Demo2_DoWhile {
	public static void main(String[] args) {
		// 在控制台输出1-10
		int i = 1;
		do {
			System.out.println("i = " + i);
			i++;
		}
		while (i <= 10);

		// while语句的无限循环
		while (true) {
			System.out.println("你好");
		
		// for语句的无限循环
		for (; ; ) {
			System.out.println("hello world");
		}
		
		}
	}
}
// do while 至少执行一次循环体。先执行后判断。这是和 while 和 for 的区别(它俩是先判断，后执行)
/*
for(int i = 1;i<10;i++) {
}  这里面的 i变量当 for语句执行完之后，就会被释放，下面的代码如果需要用到这个 i 变量，需要重新定义。
*/
