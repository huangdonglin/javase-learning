// 控制跳转语句标号

class Demo3_Mark {			// mark 标记
	public static void main(String[] args) {
		a: for (int i = 1;i <= 10 ;i++ ) {
			System.out.println("i = "+ i);
			b: for (int j = 1;j <= 10 ;j++ ) {
				System.out.println("j = "+j);
				break a;   // 指定跳出哪一层循环
			}
		}
		// 可以直接写一个空的标号，不影响程序运行
		System.out.println("大家好");
		http://www.heima.com
		System.out.println("才是真的好");
		// 能够正确输出。不要被它长得像一个网址而误导。要知道 //相当于一句注释。实际内容只有 http: 这是一个合法的标号，所以不会报错的。千万不要说成是一个网址。。
	}
}
