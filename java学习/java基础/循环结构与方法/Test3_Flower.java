/* 
	需求：在控制台输出100-999内的“水仙花数”
	例子：所谓的水仙花数指一个三位数 各位数字立方和等于数字本身，例如：153
*/

/* 
分析：
	1. 100-999
	2. 获取一个数的每一位数字
	3. 判断各个位上的立方和是否等于这个数，如果等于这个数，就打印出来
*/

class Test3_Flower {
	public static void main(String[] args) {
		for (int i = 100;i <= 999;i++) {
			int ge = i % 10;
			int shi = i / 10 % 10;
			int bai = i / 100 % 10;
			if ((ge*ge*ge + shi*shi*shi + bai*bai*bai) == i ) {          // java里面没有立方的运算符
				System.out.println(i);
			}
		}	
	}
}
 