class Demo3_for_for {      //for 语句的嵌套
	public static void main(String[] args) {
		// 输出四行五列的星星
		for (int i = 1;i <= 4 ;i++ ) {
			for (int j = 1;j <= 5 ;j++ ) {
				System.out.print("*");    // println()  输出结果后自动换行 ，print() 不会自动换行
			}
			System.out.print("\n");
			// 或者 System.out.prntln();  也可以
		}
	}
}
