class Test2_for {
	public static void main(String[] args) {
		// 求 1-100之间奇数的和
		int sum = 0,i;
		for (i = 1;i<=100;i++) {
			if (i % 2 == 0) {
				sum = sum + i;
			}
		/* 也可以这样
		for (i = 0; i<=100;i+=2);{
			sum = sum + i;
		}
		*/
		}
		System.out.println(sum);

	}
}
