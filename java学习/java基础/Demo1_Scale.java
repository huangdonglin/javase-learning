class Demo1_Scale {         // 进制
	public static void main(String[] args) {
		/* 
		* 二进制	0b    (b 可大写可小写)
		* 八进制	0
		* 十进制    
		* 十六进制  0x    (x 可大写可小写)
		*/
	//  下面分别以各类型进制形式输出
		System.out.println(0b100);
		System.out.println(0100);
		System.out.println(100);
		System.out.println(0x100);
	}
}
