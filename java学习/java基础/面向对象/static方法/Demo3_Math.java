
// Math.random()  生成0.0 到 1.0 之间的double型 伪随机数（利用算法生成的随机数，称为伪随机数）

class Demo3_Math {    
	public static void main(String[] args) {
		double d = Math.random();
		System.out.println(d);

		for (int i = 0;i < 10 ;i++ ) {
			System.out.println(Math.random());
		}

		// 生成1-100的随机数
		// Math.random() 0.0000000 - 0.999999999
		// Math.random() * 100 ======> 0.0000000 - 999999999
		// (int)(Math.random() * 100) ===> 0 - 99
		// (int)(Math.random() * 100) + 1 

		for (int i = 0;i < 10 ;i++ ) {
			System.out.println((int)(Math.random() * 100) + 1 );
		}
	}
}
