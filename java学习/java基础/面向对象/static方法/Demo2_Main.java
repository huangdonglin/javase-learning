class Demo2_Main {
	public static void main(String[] args) {
		/* 
		public : 被jvm调用，所以权限要足够大
		static : 被jvm调用，不需要创建对象，直接 类名.调用即可 
		void   : 被jvm调用，不需要有任何返回值
		main   : 不是关键字，只有这样子写才能被jvm识别
		string[] args : 以前是用来接收键盘录入的 ，这个args代表数组名，可随意起名字，但默认是args
		*/

		System.out.println(args.length);
	}
}
