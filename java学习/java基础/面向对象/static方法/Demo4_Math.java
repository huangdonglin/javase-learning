/* 
A: 案例演示：猜字游戏（数据在1-100之间） 	
*/
import java.util.Scanner;
class Demo4_Math {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("请输入一个整数，范围在1-100内");
		int guessNum = (int)(Math.random() * 100) + 1;
		while (true) {
			int result = sc.nextInt();
			if (result > guessNum) { 
				System.out.println("大啦");
			}else if (result < guessNum) {
				System.out.println("小了");
			}else {
				System.out.println("猜中了！！！");
				break;
			}
		}
	}
}
