/* 
A: 构造方法概述和作用
	* 给对象的数据(属性)进行初始化
B: 构造方法格式特点：
	* 方法名与类名相同(大小也要与类名一致)
	* 没有返回值类型，连void都没有
	* 没有具体的返回值return;  就是可以不写return
	* 构造方法不能用对象调用
C: 构造方法注意事项
	* 如果我们没有给出构造方法，系统将自动提供一个无参构造方法。
	* 如果我们给出了构造方法，系统将不再提供默认的无参构造方法。
		* 注意：这个时候，我们还想使用无参构造方法，就必须自己给出。建议永远自己给出无参构造方法。

E: 给成员变量赋值的两种方式的区别：
	第一种：就是本例中的 构造函数赋值。
	第二种：就是前面讲的 创建 setXxx()和getXxx() 两种方法。
  * 区别:
  构造方法：
  给属性进行初始化，不能修改属性值。每次调用都相当于创建新的对象。
  setXxx方法：
  修改属性值。
  这两种方式，在开发中用setXxx更多一些，比较灵活。

D: 案例演示： 构造方法的重载。(自定义有参构造函数)
		重载： 方法名相同，返回值类型无关(构造方法没有返回值),只看参数列表。

*/

class Demo1_Constructor {				// Construction 构造
	public static void main(String[] args) {
		// 调用无参系统自动提供的构造参数
		Person p1 = new Person();

		// 调用有参
		Person p2 = new Person("张三",20);
		
		
	}
}

class Person {
	private String name;
	private int age;

	// 构造方法
	public Person() {
		System.out.println("无参的构造");
		System.out.println(name + "....." + age );
		return;
	}

	//重载构造方法：
	public Person(String name,int age) {
		this.name = name;
		this.age = age;
		System.out.println("有参的构造");
		System.out.println(name + "......" + age);

	}
	
	
}