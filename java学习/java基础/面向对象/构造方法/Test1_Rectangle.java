class Test1_Rectangle {				// Rectangle 矩形
	public static void main(String[] args) {
		Rectangle r1 = new Rectangle(10,20);
		System.out.println(r1.getLength() + " " + r1.getArea());
	}
}

/* 案例演示
	需求：
		* 定义一个长方形类，定义周长和面积，
		* 然后一个测试类进行测试。
	分析：
		成员变量： 宽width 高high
		空参有参构造
		成员方法：
		setXxx和getXxx
		求周长：getLength()
		求面积：getArea()
*/
class Rectangle {
	private int width;
	private int height;

	public Rectangle() {}    // 空参构造

	public Rectangle(int width,int height) {
		this.width = width;
		this.height = height;
	}
	public void setWidth(int width) {   // 设置宽
		this.width = width;
	}

	public int getWidth() {            // 获取宽
		return width;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getHeight() {
		return height;
	}
	public void show() {
		System.out.println("长方形高：" + height + " 宽：" + width );
	}
	
	public int getLength() {				//获取周长 
		return 2 * (height + width) ;
	}

	public int getArea() {                  //获取面积
		return width * height; 
	}
}
