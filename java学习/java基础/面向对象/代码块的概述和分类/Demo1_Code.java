// 创建代码块的应用

class Demo1_Code {
	public static void main(String[] args) {
		// 局部(方法)代码块  ：   限定变量的生命周期，只能在这对大括号范围内有效，用完就会被及早的释放掉，提高内存的利用率。
		{   
		int x = 20;
		System.out.println(x);
		}

		//构造代码块(初始化块)： 在类中，方法外出现，多个构造方法中相同的代码存放到一起，每次调用构造都执行，并且在构造方法前执行
		Student s1 = new Student();
		System.out.println("-------------");
		Student s2 = new Student("张飞",23);

		/*
		静态代码块：
		 * 在类中，方法外出现，加了static修饰
		 * 用于给类进行初始化，在加载的时候就执行，并且只执行一次。
		 * 一般用于加载驱动
		 * 优先于主方法执行，先执行静态代码块，在执行类中调用的方法。
		*/
	}
	static {        // 优先于主方法进行
		System.out.println("我是主方法中的静态方法"); 
	}
}

class Student {
	private String name;
	private int age;


	public Student(){
		System.out.println("空参构造");   // 空参构造
	}      
	
	public Student(String name,int age) {
		this.name = name;
		this.age = age;
		System.out.println("有参构造");
	}

	public void setName() {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setAge() {
		this.age = age;
	}

	public int getAge() {
		return age;
	}

	// 构造代码块，每创建一次对象就会执行一次，优先于构造函数执行
	{
	System.out.println("构造代码块");
	study();
		
	}

	public void study() {
		System.out.println("学生学习");

	}

	static {      // 静态代码块 ： 随着类的加载而加载，且只执行一次 ，给类进行初始化的，一般用来加载驱动。
		System.out.println("静态代码块");
	}
}

 
