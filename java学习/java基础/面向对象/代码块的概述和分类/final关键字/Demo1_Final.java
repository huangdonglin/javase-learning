class Demo1_Final {
	public static void main(String[] args) {
		System.out.println("Hello World!");
	}
}
/* 
* A:final概述
	final： 最终的意思
* B:final修饰特点
	* 修饰类，类不能被继承
	* 修饰变量，变量就变成了常量，只能被赋值一次。
	* 修饰方法，方法不能被重写
* C: 案例演示
		* final修饰特点
*/

class Father { 
	public final void print() {     // 加上这个关键字之后，子类就不能对这个方法进行修改了。类也一样，如果类上面加上这个关键字，那么类就不能被继承。
	// 常量命名规则：如果是一个单词全部大写，如果是多个单词，那么每个单词都大写，且之间用下划线隔开。	
		System.out.println("访问底层数据资源");
	}
}

class Son extends Father {
	final int NUM = 10;
	
	public static final double PI = 3.14;     // public是共用的，谁都可以去访问，而static是静态的，说明可以用类名.去访问，与类相关的一个属性。  所以final 修饰变量叫做常量，一般会与public static共用。

	public void print() {
		NUM = 20;			// 将num赋值为10后，就不能再进行改变了，会报错。那么实际上num就变成了一个常量！所以要遵循 常量命名规范字母大写：NUM
		System.out.println("哈哈，功能被我干掉了"); 
		System.out.println(NUM);
	}
}
