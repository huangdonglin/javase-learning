// final修饰变量的初始化时机

class Demo2_Final {
	public static void main(String[] args) {
		Demo d = new Demo();
		d.print();
	}
}
/* 
*A:final 修饰变量的显示初始化时机(赋值时机)
	* 显示初始化，final int num = 10;
	* 在对象构造完毕前即可。在构造方法内。
*/
class Demo {
	final int num;       // 成员变量的默认初始化值是无效值
	//num = 10;   这样会报错，因为只能为变量赋值一次。
	public Demo() {
		num = 10;      // 可以在构造方法中对它进行赋值
	}

	public void print() {
		System.out.println(num);
	}
}
