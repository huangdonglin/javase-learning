// 继承中成员变量的关系

// this和super的区别和应用
/* 
this： 代表当前对象的引用，谁来调用我，我就代表谁
super: 代表当前父类的引用。
super.成员变量: 调用父类的成员变量。
*/
class Demo4_Extends {
	public static void main(String[] args) {
		Son s = new Son();
		s.print();
	}
}
/* 案例演示：
	* a: 不同名的变量
	* b: 同名的变量
*/
class Father {
	int num1 = 10;
    int num2 = 30;
}
	
class Son extends Father{
	int num2 = 20;	
	
	public void print() {
		System.out.println(num1);
		System.out.println(this.num1);   // this既可以调用本类的，也可以调用父类的(本类中没有的情况下)
		System.out.println(num2);   // 就近选择 ，子父类中出先相同的变量，就就近用子类的 。
		System.out.println(super.num2);  // super 调用父类的方法。
	}								// 真正的开发中不会出现子父类有同名的变量，子类继承父类就是为了使用父类的成员，那么定义了同名的成员变量在子类中就没有意义了。
}
