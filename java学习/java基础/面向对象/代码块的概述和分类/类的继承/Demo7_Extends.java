

class Demo7_Extends {
	public static void main(String[] args) {
		Son s = new Son();
		s.print();
		s.method();
	}
}

class Father {
	public void print() {
		System.out.println(" Fu print");
	}
}

class Son extends Father {
	public void method() {
		System.out.println(" Zi method");
	}

	public void print() {
		super.print();     // 调用父类的方法
		System.out.println(" Zi print");
	}
}