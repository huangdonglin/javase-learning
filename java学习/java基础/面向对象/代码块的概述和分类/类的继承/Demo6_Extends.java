// 8.10 继承中的构造方法的注意事项

class Demo6_Extends {
	public static void main(String[] args) {
		Son s1 = new Son();
		System.out.println("---------------");
		Son s2 = new Son("张三",23);
		System.out.println(s2.getName() + "..." + s2.getAge());
	}
}
/* A:案例演示
	* 父类中没有无参构造方法，子类怎么办？
	* super解决
	* this 解决
   B:注意事项
	* super(...)或者this(...)必须出现在构造语句的第一条语句上
*/
class Father {
	private int age;
	private String name;
/*
	public Father() {
		System.out.println("Father 空参构造");
	}
*/
	public Father(String name,int age) {
		this.name = name;
		this.age = age;
		System.out.println("Father 有参构造");
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

	public void setAge(int age) {
		this.age = age;
	}
	
	public int getAge() {
		return age;
	}
}

class Son extends Father {
	
	public Son(){
		super("李四",24);
		// 这里要注意，如果父类只有有参构造，系统不会自动提供无参构造，要想能成功调用父类的构造方法，就需要与其参数匹配上 比如 super("张资",23); 这样才不会报错
		System.out.println("Son 空参构造");
		//super("李四",24);
	}
	
	
	public Son(String name,int age) {
		super(name,age);
		System.out.println("Son 有参构造");
	}
} 
