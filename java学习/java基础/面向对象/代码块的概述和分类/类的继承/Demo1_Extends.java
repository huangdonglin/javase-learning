/* 类继承的弊端
	类的耦合性增强了，就是稍稍改变父类，就会影响所有子类

	开发的原则 ：高内聚，低耦合
	耦合： 类与类的关系
	内聚： 就是自己完成某件事情的能力
*/

/* java中的继承特点
	* a: java中只支持单继承，不支持多继承。(一个儿子只能有一个爹)
	* b: java支持多层继承（继承体系）。
*/
class Demo1_Extends {        // extends 继承 
	public static void main(String[] args) {
		Cat c = new Cat();
		c.color = "花";
		c.leg = 4;
		c.eat();
		c.sleep();
		System.out.println(c.leg + "..." + c.color);
	}
}
/* 
 A: 继承(extends)
	* 让类与类之间产生关系，子父类关系
*/
// 案例演示  动物类，猫类，狗类 ，定义两个属性(颜色，腿的个数)两个功能（吃饭，睡觉）

class Animal {
	String color;
	int leg;

	public void sleep() {
		System.out.println("睡觉");
	}

	public void eat() {
		System.out.println("吃饭");
	}
}

class Cat extends Animal {    // 继承父类，拿到归类里面的属性和行为
	 
}  