/* java中的继承特点
	* a: java中只支持单继承，不支持多继承。(一个儿子只能有一个爹)
	* b: java支持多层继承（继承体系）。
*/
// 子类可以使用父类中的方法，父类不能使用子类中的方法

class Demo2_Extends {
	public static void main(String[] args) {
		DemoC d = new DemoC();
		d.show();
	}
}

class DemoA {
	public void show() {
		System.out.println("DemoA");
	}
}

class DemoB extends DemoA {
	public void show() {
		System.out.println("DemoB");
	}
}

class DemoC extends DemoB{
	public void show() {
		System.out.println("DemoC");
	}
}
