
class Demo5_Extends {
	public static void main(String[] args) {
		Son s = new Son();
		
	}
}
/*	
A:案例演示：
	* 子类中的所有构造方法默认都会访问父类中空参数的构造方法，也就是说创建子类对象时，先执行父类中的空参构造，再执行子类的。
B:为什么呢？
	* 因为子类会继承父类中的数据。可能还会使用父类的数据。
	* 所以子类初始化之前，一定要先完成父类数据的初始化。

	*其实：
		* 每一个构造方法的第一条语句默认都是：supper()。 Object类是最顶层的父类，它上面没有更高的类了。
*/
class Father {      //系统默认是这样的： class Father extends Object
	public Father() {
		System.out.println("Father 的继承方法");
	}
}

class Son extends Father {
	public Son() {
		super();			// 这是一条语句，如果不写，系统会默认加上，用来访问父类中的空参构造
		System.out.println("Son 的构造方法");
	}
}

