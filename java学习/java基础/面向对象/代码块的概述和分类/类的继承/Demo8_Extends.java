// 继承中成员方法的关系
/* 
	A.重写 ：子父类中出现了同名的方法。 
	B.方法重写的应用：
		* 当子类需要父类的功能，而功能主体子类有自己特有的内容时，可以重写父类方法，这样，既沿袭了父类的功能，又定义了子类特有的内容。
	C. 案例演示 定义一个手机类
*/

/* 方法重写的注意事项
	a ：父类中私有方法不能被重写
	    *原因：子类根本无法继承父类的私有方法
	b: 子类重写父类方法时，访问权限不能更低
		* 最好就一致
	c: 父类静态方法，子类也必须通过静态方法进行重写
		* 其实这个算不上方法重写，但是现象确实如此，至于为什么算不上方法重写，多态中会讲解。
	d:自类重写父类的方法时，最好声明一模一样。

*/
class Demo8_Extends {
	public static void main(String[] args) {
		Ios8 i = new Ios8();
		i.siri();
		i.call();
	}
}

class Ios7 {
	public void call() {
		System.out.println("打电话");
	}

	public void siri() {
		System.out.println("Speak English");
	}
}

// 升级系统，使Ios8 不仅能打电话，还支持中文的siri.
class Ios8 extends Ios7 {
	public void siri() {
		System.out.println("说中文");
		super.siri();
	}
}

/* 
	方法重写和方法重载的区别：
	* 方法重载（overload）: 可以改变返回值类型，只看参数列表
	* 方法重写（override）：子类中出现和父类中方法声明一模一样的方法。与返回值类型有关，返回值是一致（或者是子父类）的

	* 方法重载：本类中出现的方法名一样，参数列表不同的方法，与返回值类型无关。
	* 子类对象调用方法的时候：
		* 先找子类本身，再找父类。
*/
