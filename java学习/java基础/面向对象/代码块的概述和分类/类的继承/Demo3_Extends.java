/* 
继承的注意事项：
	*a 子类只能继承父类中所有的非私有的成员(成员方法和成员变量)
	*b 子类不能继承父类的构造方法，但是可以通过super（马上讲）关键字去访问父类的构造方法。
	*c 不要为了部分功能而去继承
	* 项目经理 姓名 工号 工资 奖金
	* 程序员  姓名 工号 工资    

	向上面这项的假如创建两个类 项目经理和程序员 ，不能让项目经历去继承程序员，因为工资啥的都不一样，所以必须符合谁是谁的一种才行。比如 香蕉是水果的一种 ，那么香蕉可以继承自水果类. 不能说项目经理是程序员的一种。
*/
class Demo3_Extends {
	public static void main(String[] args) {
		Son s = new Son();
		s.show();
	}
}

class Father {
	public void show() {    // 当将 public 换成 static 变成私有方法或者私有变量就没办法继承了。
		System.out.println("hello world");
	}
}

class Son extends Father {

}
