/* 面向对象
B. 我们如何描述现实世界事物
		* 属性 就是该事物的描述信息(食物身上的名词)
		* 行为 就是该事物能够做什么(事物身上的动词)
C. java中最基本的单位是类，Java中用class描述事物也是如此
		* 成员变量 就是事物的属性
		* 成员方法 就是事物的行为
D. 定义类其实就是定义类的成员(成员变量和成员方法)
		* 成员变量		和以前定义变量是一样的，只不过位置发生了改变。在类中，方法外。
		* 成员方法		和以前定义方法一样的，只不过把 static 去掉，后面再详细讲解 static 的作用。
/*
	创建对象的格式：类名 对象名 = new 类名();
	对象名： 其实就是合法的标识符，如果是一个单词，所有字母小写。如果是多个单词，从第二个单词开始首字母大写

	如何使用成员变量名呢？
		* 对象名.变量名

*/
// 案例 ： 定义学生类以及使用
/* 
	属性： 姓名，年龄，性别
	行为： 学习，睡觉
*/	
class Demo1_Object {
	public static void main(String[] args) {
		Student huangDongLin = new Student();  // 创建对象
		huangDongLin.name = "张三";
		huangDongLin.age = 23;
		System.out.println(huangDongLin.name + "..." + huangDongLin.age);
		huangDongLin.study();
		huangDongLin.sleep();
	}
}

// 定义学生类
class Student {
	String name;    //对学生属性的描述
	int age;
	String gender;
	public void study() {
		System.out.println("学生爱学习");
	}

	public void sleep() {
		System.out.println("学生爱睡觉"  );
	}
}