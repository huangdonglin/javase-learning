
// 面向对象(成员内部类私有使用)

class Demo2_InnerClass {
	public static void main(String[] args) {
		
		//Outer.Inner oi =  new Outer().new Inner();    // 创建内部类对象
		//oi.method();        当变为私有变量 num的时候，不能再通过这样的方式去访问它了

		Outer o = new Outer();
		o.print();
	
	}
}

class Outer {
	private int num = 10;

	class Inner {
		public void method() {
			System.out.println(num);
		}
	}

	public void print() {
		Inner i = new Inner();
		i.method();
	}
}