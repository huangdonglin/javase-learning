// 匿名内部类重写多个方法调用

class Demo6_NoNameInnerClass {
	public static void main(String[] args) {
		Outer o = new Outer();
		o.method();
	}
}
// 匿名内部类只能针对重写调用一个方法的时候使用。重写多个方法时，就使用有名字的内部类进行。
interface Inter {
	public void show1();
	public void show2();
}

class Outer {
	public void method() {
		new Inter() {     // 实现接口
			public void show1(){ 
				System.out.println("show1");
			}
			
			public void show2() {
				System.out.println("show2");
			}

		}.show1();

		new Inter() {     // 实现接口
			public void show1(){ 
				System.out.println("show1");
			}
			
			public void show2() {
				System.out.println("show2");
			}

		}.show2();    // 可以看到调用起来很麻烦。
		
		// 下面介绍一个比较好的写法  
		Inter i = new Inter() {     // 实现接口     这里的 new Inner() {里面的内容}  就是表示一个 Inter的子类对象， 所以可以使用  Inter i = new Inner() 这种形式创建不匿名的对象 i：父类引用指向子类对象。 
			public void show1(){ 
				System.out.println("show1");
			}
			
			public void show2() {
				System.out.println("show2");
			}
			
			// 创建一个子类自己的方法
			/*
			public void hello() {     // 这个方法将不能直接调用 ： i.show3  因为变异的时候先看父类，父类中就没有 hello方法，所以编译失败。 这里也不能是用多态的强转（向下转型）来进行 ，因为没有子类对象的名字，一切操作都是 根据父类Inter 来进行的。
				System.out.println("hello world");
			}
			*/

		};
		System.out.println("-----------------");
		i.show1();
		i.show2();
	}
	//但是这么做也是有弊端的，假如子类有一个自己的方法 hello() ,那么将，没办法调用它
}