// 局部内部类访问局部变量的问题

class Demo4_InnerClass {
	public static void main(String[] args) {
		
		Outer o = new Outer();
		o.method();
		
	}
}

class Outer {
	public void method() {
		int num = 10; 
		//int num = 20;    // 加上这句话就会报错。因为java自动在 局部变量前面加上一个 final 修饰符 。具体请查看：https://www.cnblogs.com/oldpub-blog/p/9026824.html 文章，或者是听视频老师的讲解和画图。
		class Inner {     // 这里可以直接进行·访问 局部变量num ，但是老师的会报错 需要在前面加上 final
			public void print() {
				System.out.println(num);
			}
		}

		Inner i = new Inner();
		i.print();
	}
	/*                  //错误：局部内部类只能在其所在的方法中访问
	public void run() {
		Inner i = new Inner();
		i.print();
	}
	*/
}