// 静态成员内部类

class Demo3_InnerClass {

	public static void main(String[] args) {
		Outer.Inner oi = new Outer.Inner();
		oi.method();

		Outer.Inner2.print();   // 静态内部类，直接通过 类名. 就可以
	}
}

class Outer {
	static class Inner {
		public void method() {
			System.out.println("method");
		}
	}

	static class Inner2 {
		public static void print() {
			System.out.println("print");
		}
	}
}