// 匿名内部类在开发中的作用

class Test1_NoNameInnerClass {
	// 如何调用PersonDemo中的method方法呢？
	public static void main(String[] args) {
		// 普通方法解决这个问题
		PersonDemo pd = new PersonDemo();
		pd.method(new Student());

		// 匿名内部类解决
		PersonDemo pd1 = new PersonDemo();       // 匿名内部类当作参数传递（本质把匿名内部类看作一个对象）
		/* 相当于：
		Person p = new PersonDemo() {
			public void show() {
				System.out.println("匿名内部类的方法解决这个问题");
			}
		});
		*/
		pd1.method(new Person(){
			public void show() {
				System.out.println("匿名内部类的方法解决这个问题");
			}
		});

	}
}

// 这里写抽象类，接口都行
abstract class Person {
	public abstract void show();
}

class PersonDemo {
	public void method(Person p) {
		p.show();
	}
}

class Student extends Person {
	public void show() {
		System.out.println("普通继承方法使用子类解决这个问题");
	}
}

class PersonTest {
	public static void main(String[] args) {
		//p.show();
	}
}