class Demo5_NonameInner {
	public static void main(String[] args) {
		Outer o = new Outer();
		o.method();
		o.method1();
	}
}
/* 
* A: 匿名内部类 : 匿名内部类是 局部内部类的一种，必须写在方法里面。
	* 就是内部类的简化写法。
* B: 前提： 存在一个类或者接口
	* 这里的类可以是具体的类也可以是抽象的类。
* C:格式：

		new 类名或者接口名() {      // new 类名()：叫做继承这个类 ，new 接口()：叫做实现这个接口
			重写方法;
		}
* D: 本质是什么呢？
	* 是一个继承了该类或者是实现了该接口的子类匿名对象。
* E: 实例演示
	* 按照要求来一个匿名内部类
*/

interface Inter {
	public void print();
}

class Outer {
	class Inner implements Inter{
		public void print() {
			System.out.println("print");
		}
	}

	public void method() {
		Inner i = new Inner();
		i.print(); 
	}
	// 下面定义匿名内部类
	public void method1() {

		//Inter i = new Inner();  // 父类引用指向子类对象。
		new Inner().print();  // 匿名对象，创建 接口Inter的子类(Inner)的匿名对象，再调用事先重写好的子类里面的 print() 方法。
		System.out.println("-------------");
		
		// 创建 匿名内部类对象，必须是再方法内部，因为它属于是局部内部类的一种。
		new Inter() {    //实现Inter接口 : new Inter()，整个的 new Inter() {里面的内容} 叫做 Inter的子类对象(下一节还会讲这个子类对象)
			public void print() {   // 重写抽样方法
				System.out.println("匿名内部类的对象重写 print() 方法");
			} 
		}.print();	
	}
	
}