class Demo1_Constant {        // constant常量讲解
	public static void main(String[] args) {
		/*
		* 字符串常量      用双引号括起来的内容
		* 整数常量        所有整数
		* 小数常量		  所有小数
		* 字符常量		  用单引号括起来的内容，里面只能放单个数字，字母或符号
		* 布尔常量		  较为特殊，只有true和false
		* 空常量		  null（数组部分讲解）
		*/
		System.out.println("abc");
		System.out.println(123);
		System.out.println(12.3);
//		System.out.println('10');
//		System.out.println('');	    这两种都会报错
		System.out.println(' ');    // 这个可以正确执行，打印出来一个 空格
		System.out.println(true);   // bool
		System.out.println(false);
	}
}

// 和c语言不同，每个输出语句结束后，光标自动跑到下一行开头，这点和 python 一样。
